# projetodocker

Aplica testes web no site  http://automationpractice.com/, utilizando as ferramentas: selenium Web Drive e Robot Framework.

# Funcionalidades testadas
* Cadastro de usuario
* Busca por produto
* Login com sucesso (utilizando o usuario de cadastro)
* Login inválido
* Login em branco

# Executando projeto com Docker

### Requisitos 
* [Docker](https://www.docker.com/get-started)

### Construindo imagem
`docker build -t test-robot .`

### Executando testes
```
Windows -> docker run -v %cd%/report:/app/report/ test-robot
Linux/Debian -> docker run -v $(pwd)/report:/app/report/ test-robot
```

# Executando projeto sem Docker

### Requisitos de sistema
* [Phyton 3.7+](https://www.python.org/downloads/)
* Chrome 83+
* [Chromedriver 85+](https://github.com/SeleniumHQ/selenium/wiki/ChromeDriver)

### Instalando dependências
`pip install --no-cache-dir -r requirements.txt`

### Executando os testes
`robot -d ./report Tests\`

# Observações
Serão executados todos os casos de testes e os detalhes com os prints do resultado de cada caso de teste estarão disponíveis em relatório HTML no diretório /report.
