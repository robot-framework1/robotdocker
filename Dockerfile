FROM python:3.7-alpine3.9

RUN apk update
RUN apk add --no-cache \
   udev \
   chromium \
   chromium-chromedriver \
   xvfb

# Habilita display virtual
ENV DISPLAY 20
ENV SCREEN_WIDTH 1366
ENV SCREEN_HEIGHT 768
ENV SCREEN_DEPTH 16

RUN mkdir /app
WORKDIR /app

# Instala dependências do projeto
COPY requirements.txt /app/requirements.txt
RUN pip3 install --no-cache-dir -r requirements.txt

# Desabilita sandbox e GPU do Chrome
RUN sed -i "s/self._arguments\ =\ \[\]/self._arguments\ =\ \['--no-sandbox',\ '--disable-gpu'\]/" $(python -c "import site; print(site.getsitepackages()[0])")/selenium/webdriver/chrome/options.py;

COPY . /app

CMD robot -d ./report Tests