*** Settings ***
Library         SeleniumLibrary

*** Keywords ***

Dado que estou na página de login

    Go to               ${url}  

Quando eu preencho os campos de login e senha com dados validos

    Login with        ${cadastro_random_email}@email.com       123456
    Login button

Quando eu não preencho os campos de login e senha

    Login button

Quando eu preencho os campos de login e senha com dados invalidos

    Login with        usuarioincorreto         111111
    Login button

Então sou autenticado com sucesso
    
    Page Should contain Link                 http://automationpractice.com/index.php?controller=my-account        View my customer account 
    Element Text Should Be                   ${cadastro_nome_logado}         Robot Framework
    
Então vejo a mensagem informando que o usuario é invalido

    Page Should contain           Invalid email address.

Então o endereço de email é requerido

    Page Should contain           An email address required.

Login with
    [Arguments]        ${usuario}      ${login_senha}

    input text         ${email}        ${usuario} 
    input text         ${senha}        ${login_senha}

Login button
    Click Element      ${login_botao}  
       