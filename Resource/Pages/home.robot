*** Settings ***
Library         SeleniumLibrary

*** Variables ***

${home_pesquisa}                         id:search_query_top
${home_botao_pesquisa}                   name:submit_search
${home_resultado_pesquisa}               xpath:.//span[contains(text(), "dresses")]

*** Keywords ***

Dado que realizei login no site
    Wait Until Element Is Visible          ${nome_logado}   
    Element Text Should Be                 ${nome_logado}         Robot Framework

Quando pesquiso por um produto
    input text              ${home_pesquisa}             dresses
    Click Element           ${home_botao_pesquisa} 

Então o produto deve ser apresentado como resultado da pesquisa

    Wait Until Element Is Visible                ${home_resultado_pesquisa}
    Element Text Should Be                       ${home_resultado_pesquisa}        "DRESSES"

