*** Settings ***

Library             SeleniumLibrary

***Keywords***

Nova sessão
    Open Browser      about:blank        headlesschrome


Encerra sessão
    Capture Page Screenshot         
    Close Browser

Realizar login
    Set global variable         ${url}           http://automationpractice.com/index.php?controller=authentication&back=my-account 
    Set global variable         ${email}         id:email  
    Set global variable         ${senha}         id:passwd
    Set global variable         ${login_botao}         xpath:.//span[contains(., 'Sign in')]
    Set global variable         ${nome_logado}         xpath:.//span[contains(text(), "Robot Framework")]

    Open Browser                about:blank      headlesschrome
    Go to                       ${url}
    input text                  ${email}         raquelcardosodutra@gmail.com
    input text                  ${senha}         123456
    Click Element               ${login_botao}

