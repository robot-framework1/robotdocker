*** Settings ***
Resource             ../Resource/Resource.robot
Resource             ../Resource/Pages/cadastro_usuario.robot
Resource             ../Resource/Pages/login.robot
Test Setup           Nova sessão   
Test Teardown        Encerra sessão

*** Test Cases ***

Cenário: CTO1- Login Sucesso
    [Tags]     login
    Dado que estou na página de login
    Quando eu preencho os campos de login e senha com dados validos
    Então sou autenticado com sucesso

Cenário: CTO2- Login Inválido 
    [Tags]     loginInvalido
    Dado que estou na página de login
    Quando eu preencho os campos de login e senha com dados invalidos
    Então vejo a mensagem informando que o usuario é invalido

Cenário: CTO3- Login em Branco
    [Tags]     loginEmBranco
    Dado que estou na página de login
    Quando eu não preencho os campos de login e senha
    Então o endereço de email é requerido