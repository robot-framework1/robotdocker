*** Settings ***
Resource             ../Resource/Resource.robot
Resource             ../Resource/Pages/cadastro_usuario.robot
Resource             ../Resource/Pages/login.robot
Resource             ../Resource/Pages/home.robot
Test Setup            Realizar login  
Suite Teardown        Encerra sessão

*** Test Cases ***

Cenário: CTO1- Buscar por produto
    [Tags]     login
    Dado que realizei login no site
    Quando pesquiso por um produto
    Então o produto deve ser apresentado como resultado da pesquisa


